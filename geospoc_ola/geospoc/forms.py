from __future__ import print_function
from django.shortcuts import render
from django.http import HttpResponse
# from django.shortcuts import render_to_response

from django import forms
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from django.contrib.auth.forms import UserCreationForm
from django.core.files.storage import FileSystemStorage
from django.core.validators import RegexValidator
from django.core.exceptions import ValidationError
from django.http import HttpResponseRedirect
from django.db.models import Q
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
import json
#for url request
import requests
from django.db.models import Max

import logging
stdlogger = logging.getLogger(__name__)
log = logging.getLogger(__name__)

import os, sys

from .models import candidatesDetails,recruitersDetails,candidatesReview
ProjectDir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

path_static = ProjectDir+"\\upload"
path_static = ProjectDir+"\\geospoc\\static\\upload"

#creating a django form
class Geo_Form(forms.Form):
	name = forms.CharField()
	web_addr = forms.CharField()
	cv = forms.CharField()
	attactment = forms.CharField()
	like_working = forms.CharField()
	captcha = forms.CharField()
	email = forms.EmailField()

@csrf_exempt
def update_candidate(request):
	try:
		result={}
		data = request.POST.get('data')
		eq_data = json.loads(data)

		name = eq_data['name']
		email = eq_data['email']
		web_addr = eq_data['web_addr']
		cover_letter = eq_data['cover_letter']
		attachment = eq_data['attachment']
		working = eq_data['working']

		file_format = '.'+attachment.split('.')[1]

		max_id = candidatesDetails.objects.all().aggregate(Max('c_id'))
		print(max_id)
		new_file_name = name.replace(' ','_')+'_'+str(max_id['c_id__max']+1)+file_format
		print(path_static+"\\"+attachment)
		if os.path.exists(path_static+"\\"+attachment):
			os.rename(path_static+"\\"+attachment, path_static+"\\"+new_file_name)
			attachment = name.replace(' ','_')+file_format
			# print(path_static+"\\"+name.replace(' ','_')+file_format)

		result['name']=name
		result['email']=email
		result['web_addr']=web_addr
		result['cover_letter']=cover_letter
		result['attachment']=new_file_name
		result['working']=working


		if candidatesDetails.objects.filter(email=email).exists():
			result['entry']='exists'
		else:
			candidates_Details = candidatesDetails(name = name,
				email = email,
				web_address = web_addr,
				cover_letter = cover_letter,
				cv_filename = new_file_name,
				like_working = working)

			candidates_Details.save()
			result['entry']='created'

		print(result)
	except Exception as ex:
		log.error('Error in getting Candidate: %s' % ex)

	return HttpResponse(json.dumps(result), content_type='application/json')


@csrf_exempt
def save_recruiter(request):
    if request.method=='POST':
        result={}
        try:
            data = request.POST.get('data')
            eq_data = json.loads(data)

            email = eq_data['email']
            username = eq_data['username']
            raw_password = eq_data['password']

            user_exist=create_user_login(request,username,email,raw_password)

            result['status']= 'success'
            result['raw_password']= raw_password
            result['user_exist']= user_exist
        except Exception as ex:
            result['status']= 'Error : %s' % ex
        return HttpResponse(json.dumps(result), content_type='application/json')
    else:
        result['status']="Error"
        return HttpResponse(json.dumps(result), content_type='application/json')


def create_user_login(request,username,email,raw_password):
    user_exist = 0
    try:
        if not ( User.objects.filter(username=username,email=email).exists() ):
            #insert data into User model 
            if not User.objects.filter(username=username).exists():
                User.objects.create_user(username, email, raw_password)
            else:
                user_exist = 1
            

            atuser = User.objects.get(username=username)
            if not recruitersDetails.objects.filter(username = username).exists():
                recruitersDetails.objects.create(
                    username = username,
                    email = email,
                    password=raw_password
                ).save()

            print('Login Account Created')
            request.session['username'] = username
        else:
            print('Login Account Already Exists')
            user_exist = 1

    except Exception as ex:
        log.info('Error in creating user '+traceback.format_exc())

    return user_exist


@csrf_exempt
def search_recruiter(request):
    if request.method=='POST':
        result={}
        try:
            data = request.POST.get('data')
            eq_data = json.loads(data)

            username = eq_data['username']
            password = eq_data['password']
            user_exist = 0
            password_exist = 0

            if recruitersDetails.objects.filter(password=password,username=username).exists():
                password_exist = 1
                user_exist = 1
                request.session['username'] = username
                # request.session['username']
                # username = request.session.get('login')
            else:
                password_exist = 0
                user_exist = 0
                # logout(request)

            result['status']= 'success'
            result['user_exist']= user_exist
            result['password_exist']= password_exist

        except Exception as ex:
            result['status']= 'Error : %s' % ex
        return HttpResponse(json.dumps(result), content_type='application/json')
    else:
        result['status']="Error"
        return HttpResponse(json.dumps(result), content_type='application/json')

@csrf_exempt
def search_candidates(request):
	response={}
	try:
		# import pdb;pdb.set_trace()
		searchData=request.POST.get('data')
		print(searchData)
		searchData = searchData.strip()
		# searchInput=str(request.POST.get('searchInput'))
		if searchData == '':
			candidateData = candidatesDetails.objects.all()
		else:
			# candidateData = candidatesDetails.objects.filter(name__contains=searchData,email__contains=searchData,cover_letter__contains=searchData)
			candidateData = candidatesDetails.objects.filter(Q(name__contains=searchData) | Q(email__contains=searchData) | Q(cover_letter__contains=searchData))

		c_id = []
		name = []
		email = []
		web_address = []
		cover_letter = []
		cv_filename = []
		created_time = []
		# if candidateData.exist():
		for cd in candidateData:
				c_id.append(cd.c_id)
				name.append(cd.name)
				email.append(cd.email)
				web_address.append(cd.web_address)
				cover_letter.append(cd.cover_letter)
				cv_filename.append(cd.cv_filename)
				created_time.append(str(cd.created_time))

		
		response['status']= 'success'
		response['c_id']= c_id
		response['name']= name
		response['email']= email
		response['web_address']= web_address
		response['cover_letter']= cover_letter
		response['cv_filename']= cv_filename
		response['created_time']= created_time
		return HttpResponse(json.dumps(response), content_type='application/json')
	except Exception as ex:
		response['status'] = 'Error : %s' % ex
		return HttpResponse(json.dumps(response), content_type='application/json')

@csrf_exempt
def add_comments(request):
	try:
		result={}
		data = request.POST.get('data')
		eq_data = json.loads(data)

		current_cid = eq_data['current_cid']
		comments = eq_data['comments']
		username = request.session.get('username')

		result['comments']=comments
		result['current_cid']=current_cid
		result['username']=username

		candidates_instance = candidatesDetails.objects.get(c_id=current_cid)
		recruiters_instance = recruitersDetails.objects.filter(username=username)[0]

		if candidatesReview.objects.filter(c_id = candidates_instance,r_id = recruiters_instance).exists():
			result['entry']='exists'
		else:
			candidates_Review = candidatesReview(
				c_id = candidates_instance,
				r_id = recruiters_instance,
				comments = comments)

			candidates_Review.save()
			result['entry']='created'

		print(result)
	except Exception as ex:
		log.error('Error in add_comments: %s' % ex)

	return HttpResponse(json.dumps(result), content_type='application/json')



