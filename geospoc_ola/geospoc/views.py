from django.shortcuts import render
from django.http import HttpResponse
from .forms import *

import os, sys

# Create your views here.
from django.views.generic import TemplateView,ListView # Import TemplateView

# Create your views here.
# def homePageView(request):
#     return HttpResponse("Hello, World!")

# creating a home view
def home_view(request):
    context = {}
    form = Geo_Form(request.POST or None)
    context['form'] = form
    return render(request, "home.html", context)

class HomePageView(TemplateView):
    template_name = "home.html"

def upload(request):
    if request.method == 'POST':
        # print(request.POST)
        # file_name=request.POST.get('file_name')
        result={}
        try:
            handle_uploaded_file(request.FILES['file'], str(request.FILES['file']))
            result['status'] = 'Success'
            result['file_name'] = str(request.FILES['file'])
            return HttpResponse(json.dumps(result), content_type='application/json')
        except Exception as ex:
            result['status'] = 'Fail'
            return HttpResponse(json.dumps(result), content_type='application/json')

    else:
        result['status']="Error"
        return HttpResponse(json.dumps(result), content_type='application/json')

def handle_uploaded_file(file, filename):
    path_upload = 'geospoc/static/upload/'
    if not os.path.exists(path_upload):
        os.mkdir(path_upload)

    with open(path_upload + filename, 'wb+') as destination:
        for chunk in file.chunks():
            destination.write(chunk)



            