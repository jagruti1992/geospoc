from django.apps import AppConfig


class GeospocConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'geospoc'
