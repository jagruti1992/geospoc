﻿
app.config(function ($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
});

app.controller('index_controller', ['$scope','$rootScope','$location',function($scope,$rootScope,$location,$http,CONFIG,$window) {
	
	
	$scope.$watch("demoInput", function(){
		UserId = $scope.UserId;
		$.ajax({
			url:'/get_client/',
			type:'POST',
			dataType: 'json',
			data:{'mail':'mail'},
			success: function(response){
				// console.log(JSON.stringify(response));
				$scope.username=response.username
				if (response.client_id!=0) {
					// if(response.role=='Client')
					client_id=response.client_id
					role=response.role
					// else
					// 	var client_id=$rootScope.client_id
					// console.log(client_id)

					if(response.role!=undefined){
						if(response.role=="Admin" || response.role=="Partner"){
							$('.chooseFillingType').hide();
						}
					}
					
					$scope.display_logout = {"display" : ""};
					$scope.display_login = {"display" : "none"};
					$scope.Start_Now = 'File Your Return Now';
					$scope.Start_btn = 'Check your Tax Leakage for Free';

					if(response.role=='Client'){
						$.ajax({
							url:'/get_payment_info/',
							type:'POST',
							dataType: 'json',
							data:{'client_id':client_id},
							success: function(response){
								$scope.user.username=response.username
								$scope.$apply()
								// console.log(response);
								$.ajax({
									url:'/get_pan_info/',
									type:'POST',
									dataType: 'json',
									data:{'client_id':client_id},
									success: function(response){
										// console.log(JSON.stringify(response));
										issalat=response.salatclient
										db_pan = response.pan;

										p_age=response.p_age
										// console.log(p_age)
										i_year=response.i_year
										// console.log(i_year)

										if(response.name!=null){
											login_name=response.name.split('|')
											$scope.user.username=$scope.toTitleCase(login_name.join(' '))
										}
										current_url = response.url;

										if(response.url.indexOf('/salat_reg/') !== -1)
											$scope.redirect_leakage();

										$scope.$apply();
									}
								});
								if(response.paid == 1){
									$scope.subscribed = {"display" : ""};
									$scope.notsubscribed = {"display" : "none"};
								}else{
									$scope.subscribed = {"display" : "none"};
									$scope.notsubscribed = {"display" : ""};
								}
								$scope.$apply();
							}
						});
					}
				}else{
					$scope.subscribed = {"display" : "none"};
					$scope.notsubscribed = {"display" : ""};
				}

				if(role=='Client'){
					$.ajax({
						url:'/user_tracking_func/',
						type:'POST',
						dataType: 'json',
						data:{'event':'viewed','event_name':redirect_url,'client_id':client_id,'UserId':UserId},
						success: function(response){
							// console.log(response);
						}
					});
				}
				$scope.$apply();
			}
		});
	},900);


}]);

