# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models

from django.contrib.auth.models import User
from django.contrib.sessions.models import Session
from django.utils import timezone
from django.http import HttpResponse
# Create your models here.
import logging
log = logging.getLogger(__file__)

class candidatesDetails(models.Model):
	c_id = models.IntegerField(primary_key = True)
	name = models.CharField(max_length=50)
	email = models.CharField(max_length=100)
	web_address = models.CharField(max_length=100,null=True)
	cover_letter = models.CharField(max_length=1000,null=True)
	cv_filename = models.CharField(max_length=100,null=True)
	like_working = models.CharField(max_length=10,null=True)
	created_time = models.DateTimeField(default=timezone.now)
	updated_time = models.DateTimeField(auto_now=True)

class recruitersDetails(models.Model):
	r_id = models.IntegerField(primary_key = True)
	username = models.CharField(max_length=50)
	email = models.CharField(max_length=100)
	password = models.CharField(max_length=100)
	created_time = models.DateTimeField(default=timezone.now)
	updated_time = models.DateTimeField(auto_now=True)

class candidatesReview(models.Model):
	c_id = models.ForeignKey(candidatesDetails, on_delete=models.CASCADE)
	r_id = models.ForeignKey(recruitersDetails, on_delete=models.CASCADE)
	comments = models.CharField(max_length=1000,null=True)
	review = models.CharField(max_length=10,null=True)
	created = models.DateTimeField(default=timezone.now)
	updated = models.DateTimeField(default=timezone.now)