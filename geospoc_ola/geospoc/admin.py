# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

# Register your models here.
from import_export import resources

from .models import candidatesDetails,recruitersDetails,candidatesReview

class candidatesDetailsAdmin(admin.ModelAdmin):
	list_display = ('c_id','name','email','web_address','cover_letter','cv_filename',
		'like_working','created_time','updated_time')


class recruitersDetailsAdmin(admin.ModelAdmin):
	list_display = ('r_id','username','email','password','created_time','updated_time')
class candidatesReviewAdmin(admin.ModelAdmin):
	list_display = ('c_id','r_id','comments','review','created','updated')


admin.site.register(candidatesDetails, candidatesDetailsAdmin)
admin.site.register(recruitersDetails, recruitersDetailsAdmin)
admin.site.register(candidatesReview, candidatesReviewAdmin)
