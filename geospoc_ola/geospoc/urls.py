# pages/urls.py
from django.urls import path
from django.conf.urls import url
from .views import *
from . import views,forms

urlpatterns = [
    # path("", homePageView, name="home"),
    # path("", home_view, name="home"),
    url(r'^$',views.HomePageView.as_view(),name='index'),
    url(r'^update_candidate/',forms.update_candidate,name='update_candidate'),
    # url(r'^file_upload/',forms.fileupload,name='file_upload'),
    url(r'^upload/', views.upload, name="upload"),
    url(r'^save_recruiter/',forms.save_recruiter,name='save_recruiter'),
    url(r'^search_recruiter/',forms.search_recruiter,name='search_recruiter'),
    url(r'^search_candidates/',forms.search_candidates,name='search_candidates'),
    url(r'^add_comments/',forms.add_comments,name='add_comments'),
]